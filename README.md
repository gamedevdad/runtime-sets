# Runtime Sets

A unity module for keeping track of components in the scene at runtime. 

The pattern was popularized by [this talk](https://www.youtube.com/watch?v=raQ3iHhE_Kk).

This module solves the simple problem of having to keep track of a group of gameobjects at runtime. 
Consider an example of a radar tracking enemies (Also supplied here). 
Constantly Finding the enemies `GameObject.FindObjects<Enemy>` is an expensive update call. 
Instead the runtime set contains all active `Enemies` without the need for this call.

## Getting started
In Unity explorer create a `Runtime Set` ScriptableObject(e.g. Enemies). 
This Set the collection of current gameobjects in the scene.

<img src = "CreateSet.PNG">

Then create a `Runtime Set Manager` in the scene (e.g. enemy manager)
. The manager should be set to the `Runtime set`.
The manager is acts as a scene access point to the set information.

<img src='CreateSetManager.PNG'>

Finally add a runtime `Runtime Item` to the game objects you would like to
add to the set and set the `Runtime Set`.
This scripts adds the item to the set when it starts and removes it when it is destroyed.

<img src = 'CreateItem.PNG'>

There is a sample of creating a radar using runtime sets.
[Video](RadarVid.webm) 
