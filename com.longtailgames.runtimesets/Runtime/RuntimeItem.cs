﻿using runtime_sets.com.longtailgames.runtimesets.Runtime;
using UnityEngine;

namespace com.longtailgames.runtimesets
{
    public class RuntimeItem : MonoBehaviour
    {
        [SerializeField] private RuntimeSet set;

        private void OnEnable()
        {
            set.Register(gameObject);
        }

        private void OnDisable()
        {
            set.Deregister(gameObject);
        }
    }
}